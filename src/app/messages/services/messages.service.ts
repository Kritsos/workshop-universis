import { Injectable } from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ConfigurationService} from '@universis/common';
import * as moment from 'moment';
import _date = moment.unitOfTime._date;
import {forEach} from '@angular/router/src/utils/collection';


@Injectable()
export class MessagesService {

  constructor(private _context: AngularDataContext, private _configuration: ConfigurationService) {
  }

  getAllMessages(take, skip): any {
    return this._context.model('students/me/messages')
      .asQueryable()
      .expand('recipient, attachments,action($expand=actionStatus), sender')
      .orderByDescending('dateCreated')
      .skip(skip)
      .take(take)
      .getList();
  }

  getUnreadMessages(take, skip): any {
    return this._context.model('students/me/messages')
      .asQueryable()
      .expand('recipient, attachments, action($expand=actionStatus), sender')
      .orderByDescending('dateCreated')
      .where('dateReceived').equal(null)
      .take(take)
      .skip(skip)
      .getList();
  }

  getMessagesByDate(take, skip, currentDate: string) {
    return this._context.model('students/me/messages')
      .asQueryable()
      .expand('recipient,attachments, action($expand=actionStatus), sender')
      .orderByDescending('dateCreated')
      .where('dateCreated').greaterOrEqual(currentDate)
      .take(take)
      .skip(skip)
      .getList();
  }

  searchMessages(searchText: string, skip, take) {
    return this._context.model(`students/me/messages`)
      .asQueryable().expand('recipient, attachments, action($expand=actionStatus), sender')
      .where('subject').contains(searchText)
      .or('action/additionalType').contains(searchText)
      .or('body').contains(searchText)
      .orderByDescending('dateCreated')
      .skip(skip)
      .take(take)
      .getList();
  }

  getCountOfUnreadMessages(): any {
    return this._context.model('students/me/messages')
      .asQueryable()
      .select('count(id) as total')
      .where('dateReceived').equal(null)
      .getItems();
  }

  setMessageAsRead(messageId) {
    return this._context.model(`students/me/messages/${messageId}/markAsRead`).save(null);
  }

  downloadFile(attachment) {
    console.log(attachment)
    const headers = new Headers();
    const serviceHeaders = this._context.getService().getHeaders();
    Object.keys(serviceHeaders).forEach((key) => {
      if (serviceHeaders.hasOwnProperty(key)) {
        headers.set(key, serviceHeaders[key]);
      }
    });
    const attachURL = attachment.url.replace(/\\/g, '/').replace('/api', '');

    const fileURL = this._context.getService().resolve(attachURL);
    fetch(fileURL, {
      headers: headers,
      credentials: 'include'
    }).then((response) => {

      return response.blob();
    })
      .then(blob => {
        const objectUrl = window.URL.createObjectURL(blob);
        const a = document.createElement('a');
        document.body.appendChild(a);
        a.setAttribute('style', 'display: none');
        a.href = objectUrl;
        a.download = `${attachment.name}`;
        a.click();
        window.URL.revokeObjectURL(objectUrl);
        a.remove();
      });
  }
}
