import {Component, OnInit} from '@angular/core';
import {ProfileService} from '../../../profile/services/profile.service';

@Component({
    selector: 'app-registrations-home',
    templateUrl: './registrations-home.component.html',
    styleUrls: ['./registrations-home.component.scss']
})

export class RegistrationsHomeComponent implements OnInit {
    public useStudentRegisterAction = true;
    public isLoading = true;
    constructor(private _profileService: ProfileService) { }

    async ngOnInit() {
        // get student
        const student = await this._profileService.getStudent();
        if (student) {
            // set use register action flag
            this.useStudentRegisterAction = student &&
                student.department &&
                student.department.organization &&
                student.department.organization.instituteConfiguration &&
                student.department.organization.instituteConfiguration.useStudentRegisterAction;
        }
        this.isLoading = false;
    }

}
